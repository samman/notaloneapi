const router = require("express").Router();
const authController = require("../controllers/authController");
const postController = require("../controllers/postController");
const userCommunityController = require("../controllers/userCommunityController");

router
  .route("/")
  .post(
    authController.protect,
    userCommunityController.isCommunityMember,
    postController.store
  );

router
  .route("/:post_id")
  .delete(authController.protect, postController.destroy);

router
  .route("/:post_id/uploadPostImage")
  .post(
    authController.protect,
    postController.storePostImage,
    postController.setPostImage
  );

router
  .route("/getJoinedCommunityPosts")
  .get(authController.protect, postController.getJoindCommunityPosts);

router
  .route("/:post_id/like")
  .post(authController.protect, postController.likePost)
  .delete(authController.protect, postController.unlikePost);

router
  .route("/:post_id/flag_inappropriate")
  .patch(authController.protect, postController.flag);

router
  .route("/:post_id/flag/:flag_id")
  .delete(authController.protect, postController.revokeFlag);

router
  .route("/:post_id/flag/:flag_id/vote_in_favor")
  .post(authController.protect, postController.vote);

router
  .route("/:post_id/flag/:flag_id/vote/")
  .delete(authController.protect, postController.revokeVote);

router
    .route("/:post_id/comment")
    .post(authController.protect, postController.postComment);

module.exports = router;
