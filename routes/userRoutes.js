const router = require('express').Router();

const authController = require('../controllers/authController');
const userController = require('../controllers/userController');

// router
//     .route("")
//     .get(authController.protect, userController.getUserDetails)

router
    .route("/updateMe")
    .patch(userController.updateMe)

router
    .route("/updateProfilePic")
    .patch(
        authController.protect, 
        userController.storeProfilePic,
        userController.setProfilePic
    )

module.exports = router;