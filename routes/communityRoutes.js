const router = require("express").Router();
const authController = require("../controllers/authController");
const communityController = require("../controllers/communityController");
const userCommunityController = require("../controllers/userCommunityController");

router
  .route("/")
  .get(authController.protect, communityController.index)
  .post(authController.protect, communityController.store);

router
  .route("/:id")
  .get(authController.protect, communityController.show)
  .patch(authController.protect, communityController.update)
  .delete(authController.protect, communityController.destroy);

router
  .route("/:id/updateCommunityLogo")
  .patch(
    authController.protect,
    communityController.storeCommunityLogo,
    communityController.setCommunityLogo
  );

router
  .route("/:id/join")
  .post(authController.protect, userCommunityController.joinCommunity);

router
  .route("/:id/leave")
  .post(authController.protect, userCommunityController.leaveCommunity);

module.exports = router;
