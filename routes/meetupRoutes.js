const router = require('express').Router();
const meetupController = require('../controllers/meetupController');
const authController = require('../controllers/authController');

router
    .route('/')
    .get(authController.protect, meetupController.getJoindCommunityMeetups)
    .post(authController.protect, meetupController.store);

router
    .route('/:meetupId')
    .patch(authController.protect, meetupController.update)

module.exports = router;