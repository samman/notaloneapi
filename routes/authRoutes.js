const router = require('express').Router();

const authController = require('../controllers/authController')
const userController = require('../controllers/userController');

router
    .route('/signup')
    .post(authController.signup)

router
    .route('/login')
    .post(authController.login)

router
    .route('/profile')
    .get(authController.protect, userController.getProfile)

router
    .route('/verify/:verificationToken')
    .get(authController.verify)

router
    .route('/sendVerificationEmail')
    .post(authController.protect, authController.sendVerificationEmail)

module.exports = router;