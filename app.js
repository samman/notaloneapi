const express = require("express");
const cors = require("cors");
const morgan = require("morgan");

const authRouter = require("./routes/authRoutes.js");
const userRouter = require("./routes/userRoutes");
const communityRouter = require("./routes/communityRoutes");
const postRouter = require("./routes/postRoutes");
const meetupRouter = require("./routes/meetupRoutes");

const errorHandler = require("./controllers/errorHandler");

const app = express();

app.use(cors());
app.use(express.json());

if (process.env.NODE_ENV == "development") {
  app.use(morgan("dev"));
}

const BASE_ROUTE = "/api/v1";

app.use(`${BASE_ROUTE}/auth`, authRouter);
app.use(`${BASE_ROUTE}/users`, userRouter);
app.use(`${BASE_ROUTE}/communities`, communityRouter);
app.use(`${BASE_ROUTE}/posts`, postRouter);
app.use(`${BASE_ROUTE}/meetups`, meetupRouter);

app.use(`/storage`, express.static(`${__dirname}/storage`));

app.use(errorHandler);

module.exports = app;
