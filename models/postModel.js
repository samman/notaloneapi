const mongoose = require("mongoose");

const flagSchema = new mongoose.Schema({
  flag: {
    type: String,
    default: "inappropriate",
  },
  reason: {
    type: String,
  },
  flagged_by: {
    type: mongoose.Types.ObjectId,
    ref: "User",
    required: true,
  },
  flagged_date: {
    type: Date,
    default: Date.now(),
  },
  votes: [
    {
      vote_by: {
        type: mongoose.Types.ObjectId,
        ref: "User",
      },
    },
  ],
});

const PostSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "Post title is required"],
  },
  content: String,
  post_image: String,
  community: {
    type: mongoose.Types.ObjectId,
    ref: "Community",
    required: [true, "Community must be specified for a post"],
  },
  author: {
    type: mongoose.Types.ObjectId,
    ref: "User",
  },
  users_that_liked: [
    {
      type: mongoose.Types.ObjectId,
      ref: "User",
    },
  ],
  flags: [flagSchema],
  comments: [
    new mongoose.Schema({
      content: {
        type: String,
        required: [true, "Content for a comment is required."],
      },
      author: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: [true, "Author for a comment is required."],
      },
    }),
  ],
});

module.exports = mongoose.model("Post", PostSchema);
