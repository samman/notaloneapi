const mongoose = require("mongoose");

const communitySchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Community name is required"],
        unique: [true, "Community name is already taken"]
    },
    description: String,
    privacy: {
        type: String,
        enum: ["private", "public"],
        required: [true, "Community privacy is required"]
    },
    leader: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        require: [true, "Community leader is required"]
    },
    interest_tags: {
        type: [String],
        min: [1, "Community request at leat one interest"]
    },
    community_logo: String
})

const communityModel = mongoose.model("Community", communitySchema)

module.exports = communityModel;