const mongoose = require('mongoose');

const UserCommunitySchema = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'User must be specified']
    },
    community_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Community',
        required: [true, 'Community must be specified']
    }
})

const UserCommunityModel = mongoose.model('UserCommunity', UserCommunitySchema);

module.exports = UserCommunityModel;