const mongoose = require("mongoose");

const MeetupSchema = new mongoose.Schema({
  channel_name: {
    type: String,
    required: true,
  },
  purpose: {
    type: String,
  },
  start_time: {
    type: Date,
    required: true,
  },
  duration: String,
  community_id: {
    type: mongoose.Types.ObjectId,
    ref: "Community",
  },
  convener_id: {
    type: mongoose.Types.ObjectId,
    ref: "User"
  },
  cancelled: {
    type: Boolean,
    default: false
  }
});

module.exports = mongoose.model("Meetup", MeetupSchema);
