const multer = require('multer');
const AppError = require('./appError');

module.exports = (file,dest,options)=>{
    const multerStorage = multer.diskStorage({
        destination: (req,file,cb)=>{
            cb(null, `storage/${dest}`);
        },
        filename: (req,file,cb)=>{
            // user-83298nsh99hh2332jb.jpeg
            const ext = file.mimetype.split("/")[1];
            cb(null, `${options.fileFor}-${req.user._id||req.params.id}-${Date.now()}.${ext}`);
        }
    });
    
    const multerFilter = (req, file, cb) => {
        if(file.mimetype.startsWith("image")){
            cb(null, true)
        }else{
            cb(new AppError("Uploaded file must be an Image", 400), false)
        }
    }
    
    const upload = multer({
        storage: multerStorage,
        fileFilter: multerFilter
    })
    
    return upload.single(file);
}
