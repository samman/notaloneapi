const nodemailer = require("nodemailer");
const pug = require("pug");
const { htmlToText } = require("html-to-text");

module.exports = class Email {
  constructor(user, url) {
    this.to = user.email;
    this.name = user.name;
    this.url = url;
    this.form = `Samman Adhikari <${process.env.EMAIL_FROM}>`;
  }

  emailTransport() {
    if (process.env.NODE_ENV === "production") {
      // Sendgrid
      return 1;
    }
    return nodemailer.createTransport({
      host: process.env.EMAIL_HOST,
      post: process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
      },
    });
  }

  async send(template, subject) {
    // Based on the pug template, render HTML
    const html = pug.renderFile(
      `${__dirname}/../views/emails/${template}.pug`,
      {
        filename: this.name.split(" ")[0],
        url: this.url,
        subject,
      }
    );
    // Set email options
    const options = {
      form: this.form,
      to: this.to,
      subject,
      html,
      text: htmlToText(html),
    };
    // Send email by creating transport
    await this.emailTransport().sendMail(options);
  }

  async sendVerifyEmail() {
    await this.send(
      "verifyEmail",
      "Please verify your email by clicking the button to activate your account."
    );
  }

  //   async sendPasswordReset() {
  //     await this.send();
  //   }
};
