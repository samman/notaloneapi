const catchAsync = require("../utils/catchAsync");
const UserCommunity = require("../models/userCommunityModel");
const AppError = require("../utils/appError");

// Set the requesting user as a community member
exports.joinCommunity = catchAsync(async (req, res) => {
  const joinedUser = await UserCommunity.create({
    community_id: req.params.id,
    user_id: req.user._id,
  });
  res.status(200).json({
    status: "success",
    user: joinedUser,
  });
});

// Removes the request from community members
exports.leaveCommunity = catchAsync(async (req, res) => {
  await UserCommunity.deleteMany({
    community_id: req.params.id,
    user_id: req.user._id,
  });
  res.status(204).json();
});

// Middleware to check of a requesting user if part of a community or not
exports.isCommunityMember = catchAsync(async (req, res, next) => {
  const exists = await UserCommunity.exists({
    community_id: req.body.community,
    user_id: req.user._id,
  });
  if (!exists) {
    return next(
      new AppError(
        "Only community members can make a post to the community",
        400
      )
    );
  }
  next();
});
