const catchAsync = require('../utils/catchAsync');
const imageUploader = require('../utils/imageUploader');

const User = require('../models/userModel');

exports.storeProfilePic = imageUploader("profilePic","images/profile", {
    fileFor: 'user'
});

exports.setProfilePic = catchAsync(async (req,res)=>{
    const user = await User.findByIdAndUpdate(req.user._id, { profilePic:  req.file.path }, {
        new: true,
        runValidation: true
    });
    res.status(200).json({
        status: "success",
        user
    })
})

exports.updateMe = catchAsync(async(req,res)=>{
    res.status(200).send("Successfully updated")
})

exports.getProfile = catchAsync(async(req,res)=>{
    const user = {...req.user._doc};
    delete user.role;

    res.status(200).json({
        status: "success",
        profile: user
    })
})