const mongoose = require("mongoose");
const Meetup = require("../models/meetupModel");
const UserCommunity = require("../models/userCommunityModel");
const catchAsync = require("../utils/catchAsync");
const ApiFeatures = require("../utils/apiFeatures");

exports.getJoindCommunityMeetups = catchAsync(async (req, res) => {
  const joinedCommunity = await UserCommunity.find({ user_id: req.user._id });
  const communities = joinedCommunity.map((comm) => comm.community_id);
  const apiFeature = new ApiFeatures(
    Meetup.find({ community_id: { $in: communities } }),
    req.query
  )
    .filter()
    .sort()
    .limitFields()
    .paginate();
  const meetups = await apiFeature.dbQuery;
  res.status(200).json({
    status: "success",
    meetups,
  });
});

exports.store = catchAsync(async (req, res) => {
  const meetup = await Meetup.create({
    ...req.body,
    convener_id: req.user._id,
  });
  res.status(201).json({
    status: "success",
    meetup,
  });
});

exports.update = catchAsync(async (req, res) => {
  console.log(req.params.meetupId, req.body);
  const meetup = await Meetup.findByIdAndUpdate(req.params.meetupId, req.body, {
    new: true,
    runValidators: true,
  });
  res.status(200).json({
    status: "success",
    meetup,
  });
});
