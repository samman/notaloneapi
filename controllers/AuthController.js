const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");

const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");

const User = require("../models/userModel");
const Email = require("../utils/email");

function createToken(id) {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
}

function createEmailVerificationToken(id) {
  return jwt.sign({ id }, process.env.EMAIL_VERIFICATION_JWT_SECRET, {
    expiresIn: process.env.EMAIL_VERIFICATION_JWT_EXPIRES_IN,
  });
}

exports.signup = catchAsync(async (req, res) => {
  const _id = mongoose.Types.ObjectId();
  const user = await User.create({
    _id,
    name: req.body.name,
    username: req.body.username,
    email: req.body.email,
    password: req.body.password,
    passwordConfirm: req.body.passwordConfirm,
    gender: req.body.gender,
    address: req.body.address,
    emailVerificationToken: createEmailVerificationToken(_id),
  });
  res.status(201).json({
    status: "success",
    user,
  });
});

exports.login = catchAsync(async (req, res, next) => {
  if (!req.body.username || !req.body.password) {
    return next(new AppError("Username and password requried", 400));
  }
  const user = await User.findOne({ username: req.body.username }).select(
    "+password"
  );
  if (
    !user ||
    !(await user.correctPassword(req.body.password, user.password))
  ) {
    return next(new AppError("Incorrect username or password", 404));
  }
  const token = createToken(user._id);
  res.status(200).json({
    status: "success",
    token,
    role: user.role,
  });
});

// Authentication
exports.protect = catchAsync(async (req, res, next) => {
  if (
    !req.headers.authorization ||
    !req.headers.authorization.startsWith("Bearer ")
  ) {
    return next(
      new AppError("Please provide a valid authentication token", 401)
    );
  }

  const token = req.headers.authorization.split(" ")[1];

  const decoded = jwt.verify(token, process.env.JWT_SECRET);

  const userId = decoded.id;

  const user = await User.findById(userId);

  if (!user) {
    return next(new AppError("User doesn't exist", 401));
  }

  req.user = user;
  next();
});

//Authorization
exports.restrictTo = (...roles) => {
  return (req, res, next) => {
    for (let role of roles) {
      if (role == req.user.role) {
        return next();
      }
    }
    return next(
      new AppError("Your are not authorized to perform this task"),
      403
    );
  };
};

exports.sendVerificationEmail = catchAsync(async (req, res) => {
  const email = new Email(req.user, "www.facebook.com").sendVerifyEmail();
  res.status(200).json({
    status: "success",
    message: "Please check your email",
  });
});

exports.verify = catchAsync(async (req, res) => {
  const verificationToken = req.params.verificationToken;

  const decoded = jwt.verify(
    verificationToken,
    process.env.EMAIL_VERIFICATION_JWT_SECRET
  );

  const userId = decoded.id;

  const user = await User.findById(userId);

  if (!user) {
    return next(new AppError("User doesn't exist", 401));
  }

  user.active = true;
  user.save();

  const token = createToken(user._id);
  res.status(200).json({
    status: "success",
    token,
    role: user.role,
  });
});
