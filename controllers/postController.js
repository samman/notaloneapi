const UserCommunity = require("../models/userCommunityModel");
const Post = require("../models/postModel");
const catchAsync = require("../utils/catchAsync");
const imageUploader = require("../utils/imageUploader");
const ApiFeatures = require("../utils/apiFeatures");
const AppError = require("../utils/appError");

// Store post
exports.store = catchAsync(async (req, res, next) => {
  const post = await Post.create({ ...req.body, author: req.user._id });
  res.status(201).json({
    status: "success",
    post,
  });
});

// Store post image
exports.storePostImage = imageUploader("post_image", "images/postImage", {
  fileFor: "post",
});

// Set the location of the post image to that post document
exports.setPostImage = catchAsync(async (req, res) => {
  const post = await Post.findByIdAndUpdate(
    req.params.post_id,
    {
      post_image: req.file.path,
    },
    {
      new: true,
      runValidators: true,
    }
  );
  res.status(200).json({
    status: "success",
    post,
  });
});

exports.getJoindCommunityPosts = catchAsync(async (req, res) => {
  const joinedCommunity = (
    await UserCommunity.find({ user_id: req.user._id })
  ).map((doc) => doc.community_id);
  const apiFeature = new ApiFeatures(
    Post.find({ community: { $in: joinedCommunity } })
      .populate("community")
      .populate({ path: "comments", populate: { path: "author" } }),
    req.query
  )
    .filter()
    .sort()
    .limitFields()
    .paginate();
  const posts = await apiFeature.dbQuery;
  res.status(200).json({
    status: "success",
    posts,
  });
});

exports.likePost = catchAsync(async (req, res) => {
  const post = await Post.findByIdAndUpdate(
    req.params.post_id,
    {
      $push: { users_that_liked: req.user._id },
    },
    {
      new: true,
      runValidators: true,
    }
  );
  res.status(200).json({
    status: "success",
    post,
  });
});

exports.unlikePost = catchAsync(async (req, res) => {
  const post = await Post.findByIdAndUpdate(
    req.params.post_id,
    {
      $pull: { users_that_liked: req.user._id },
    },
    {
      new: true,
      runValidators: true,
    }
  );
  res.status(200).json({
    status: "success",
    post,
  });
});

exports.destroy = catchAsync(async (req, res, next) => {
  const post = await Post.findById(req.params.post_id).populate("community");
  if (
    !post.author.equals(req.user._id) &&
    !post.community.leader.equals(req.user._id)
  ) {
    return next(new AppError("A post can only be removed by its author.", 400));
  }
  await Post.findByIdAndDelete(req.params.post_id);
  res.status(204).send();
});

exports.flag = catchAsync(async (req, res) => {
  const post = await Post.findByIdAndUpdate(
    req.params.post_id,
    {
      $push: { flags: { flagged_by: req.user._id, reason: req.body.reason } },
    },
    {
      new: true,
    }
  );
  res.status(200).json({
    status: "success",
    post,
  });
});

exports.revokeFlag = catchAsync(async (req, res, next) => {
  const post = await Post.findById(req.params.post_id);
  const flag = post.flags.find((flag) => (flag._id = req.params.flag_id));
  if (!flag.flagged_by.equals(req.user._id)) {
    return next(
      new AppError("You are not authorized to perform this action.", 401)
    );
  }
  post.flags.pull(req.params.flag_id);
  await post.save();
  res.status(204).send();
});

exports.vote = catchAsync(async (req, res) => {
  const post = await Post.findById(req.params.post_id);
  post.flags.id(req.params.flag_id).votes.push({ vote_by: req.user._id });
  await post.save();
  res.status(200).json({
    status: "success",
    post,
  });
});

exports.revokeVote = catchAsync(async (req, res, next) => {
  const post = await Post.findById(req.params.post_id);
  const vote = post.flags
    .id(req.params.flag_id)
    .votes.find((vote) => vote.vote_by.equals(req.user._id));
  console.log(req.user._id);
  if (!vote.vote_by.equals(req.user._id)) {
    return next(
      new AppError("You are not authorized to perform this action.", 401)
    );
  }
  vote.remove();
  await post.save();
  res.status(204).send();
});

exports.postComment = catchAsync(async (req, res, next) => {
  const post = await Post.findById(req.params.post_id);
  post.comments.push({ content: req.body.content, author: req.user._id });
  await post.save();
  res.status(200).json({
    status: "success",
    post,
  });
});
