const AppError = require("../utils/appError");

module.exports = (err, req, res, next) => {
  let errCopy = Object.assign(err);

  if (errCopy.name == "JsonWebTokenError")
    errCopy = handleJsonWebTokenError(errCopy);
  if (errCopy.name == "ValidationError")
    errCopy = handleValidationError(errCopy);
  if (errCopy.code == 11000) errCopy = handleDuplicateKeyError(errCopy);

  if (process.env.NODE_ENV == "development") {
    return sendDevError(errCopy, res);
  } else {
    return sendProdError(errCopy, res);
  }
};

function sendDevError(err, res) {
  const statusCode = err.statusCode || 500;
  const status = err.status || "error";
  const message = err.message;
  console.log(err);
  res.status(statusCode).json({
    status,
    message,
  });
}

function sendProdError(err, res) {
  if (!err.isOperational) {
    return res.status(500).json({
      status: "err",
      message: "Opps! Something went wrong",
    });
  }
  const statusCode = err.statusCode || 500;
  const status = err.status || "error";
  const message = err.message;
  res.status(statusCode).json({
    status,
    message,
  });
}

function handleJsonWebTokenError(err) {
  const message = "Invalid token";
  return new AppError(message, 400);
}

function handleValidationError(err) {
  let message = "Validation Error: ";
  for (let arr of Object.values(err.errors)) {
    if (arr.properties) {
      message += arr.properties.message + ".";
    } else {
      message += arr.message;
    }
  }
  return new AppError(message, 400);
}

function handleDuplicateKeyError(err) {
    console.log("..............................", err.errmsg)
  const value = err.errmsg.match(/([""])(\\?.)*?\1/)[0];
  const message = `Duplicate field value: ${value}. Please use another value!`;
  console.log(message);
  return new AppError(message, 400);
}