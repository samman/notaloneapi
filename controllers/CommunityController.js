const catchAsync = require("../utils/catchAsync");
const ApiFeatures = require("../utils/apiFeatures");
const Community = require("../models/communityModel");
const UserCommunity = require("../models/userCommunityModel");
const imageUploader = require("../utils/imageUploader");

// Get all communites
exports.index = catchAsync(async (req, res) => {
  const searchTerm = req.query.searchTerm;
  const apiFeqture = new ApiFeatures(
    Community.find({ name: new RegExp(searchTerm) }).populate("leader"),
    req.query
  )
  .sort()
  .limitFields()
  .paginate();
  const communities = await apiFeqture.dbQuery;

  for (let community of communities) {
    // Checks if the requesting user is a member of the community or not
    community._doc.is_member = !!(await UserCommunity.findOne({
      user_id: req.user._id,
      community_id: community._id,
    }));
  }

  res.status(200).json({
    status: "success",
    communities,
  });
});

// Get a particular community
exports.show = catchAsync(async (req, res) => {
  // Gets a particular community based on the community id passed in the parameter
  const community = await Community.findById(req.params.id).populate("leader");

  // Checks if the requesting user is a member of the community or not
  community._doc.is_member = !!(await UserCommunity.findOne({
    user_id: req.user._id,
    community_id: community._id,
  }));

  // Sends response
  res.status(200).json({
    status: "success",
    community,
  });
});

// Create a new community
exports.store = catchAsync(async (req, res) => {
  const community = await Community.create({
    ...req.body,
    leader: req.user.id,
  });

  // Sets the community leaders as one of the community members
  await UserCommunity.create({
    user_id: req.user._id,
    community_id: community._id,
  });

  res.status(201).json({
    status: "success",
    community,
  });
});

// Update an existing community
exports.update = catchAsync(async (req, res) => {
  const community = await Community.findByIdAndUpdate(req.params.id, req.body, {
    runValidators: true,
    new: true,
  });
  res.status(200).json({
    status: "success",
    community,
  });
});

// Delete an existing community
exports.destroy = catchAsync(async (req, res) => {
  const community = await Community.findByIdAndDelete(req.params.id);
  res.status(204).send();
});

// Store community logo
exports.storeCommunityLogo = imageUploader(
  "community_logo",
  "images/communityLogo",
  {
    fileFor: "community",
  }
);

// Set the location of the community logo to that community document
exports.setCommunityLogo = catchAsync(async (req, res) => {
  const community = await Community.findByIdAndUpdate(
    req.params.id,
    { community_logo: req.file.path },
    {
      new: true,
      runValidators: true,
    }
  );
  res.status(200).json({
    status: "success",
    community,
  });
});
