const dotenv = require("dotenv");
const mongoose = require("mongoose");

process.on('uncaughtException', err => {
    console.log("UNCAUGHT EXCEPTION: ");
    console.log(`${err}`);
    process.exit(1);
})

dotenv.config({
    path: `${__dirname}/.env`
});

mongoose.connect(process.env.DB_CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
    
},(err) => {
    if(err){
        if(process.env.NODE_ENV == 'development'){
            console.log(err);
        }else{
            console.log("DATABASE CONNECTION FAILED");
        }
        process.exit(1);
    }
    console.log("Database successfully connected.")
})

const app = require("./app");

const port = process.env.PORT || 3000;

app.listen(port, process.env.HOSTNAME, ()=>{
    console.log(`Listening to port ${port}.`)
})


process.on("unhandledRejection", err => {
    if(process.env.NODE_ENV="development"){
        console.log(err)
    }else{
        console.log("UNHANDLED PROMISE REJECTION: ")
        console.log(`${err.name}: ${err.message}`);
    }
    server.close(()=>{
        process.exit(1);
    });
})
